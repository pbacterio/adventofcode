#!/usr/bin/env python3
import re

SYMBOLS = set("#$%&*+-/=@")


def part1(input):
    result = 0
    lines = [line.strip() for line in input]
    for row, line in enumerate(lines):
        for match in re.finditer(r"\d+", line):
            start, end = match.span()
            start_prefix = line[start - 1] if start > 0 else ""
            if start_prefix in SYMBOLS:
                result += int(line[start:end])
                continue
            end_suffix = line[end] if end < len(line) else ""
            if end_suffix in SYMBOLS:
                result += int(line[start:end])
                continue

            prev_line = lines[row - 1] if row > 0 else "."
            if re.match(
                ".*[\\#\\$%\\&\\*\\+\\-/=@].*",
                prev_line[max(start - 1, 0) : min(end + 1, len(prev_line))],
            ):
                result += int(line[start:end])
                continue
            next_line = lines[row + 1] if row < len(lines) - 1 else "."
            if re.match(
                ".*[\\#\\$%\\&\\*\\+\\-/=@].*",
                next_line[max(start - 1, 0) : min(end + 1, len(next_line))],
            ):
                result += int(line[start:end])
                continue
    return result


def part2(input):
    result = 0
    lines = [line.strip() for line in input]
    for row, line in enumerate(lines):
        for match in re.finditer("\*", line):
            print(match)
    return result


if __name__ == "__main__":
    print(part1(open("input.txt")))
    print(part2(open("input.txt")))


from io import StringIO
from unittest import TestCase


class TestStringMethods(TestCase):
    def test_part1(self):
        test_input = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."""
        self.assertEqual(4361, part1(StringIO(test_input)))

    def test_part2(self):
        test_input = """"""
        assert 2286 == part2(StringIO(test_input))
