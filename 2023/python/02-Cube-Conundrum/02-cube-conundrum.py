#!/usr/bin/env python3
from collections import defaultdict
import re


def part1(input):
    result = 0
    for line in input:
        game_num, cube_sets = parse_game(line)
        valid_game = True
        for cube_set in cube_sets:
            valid_game &= is_valid(cube_set)
        if valid_game:
            result += game_num
    return result


def is_valid(cube_set):
    if cube_set["red"] > 12:
        return False
    if cube_set["green"] > 13:
        return False
    if cube_set["blue"] > 14:
        return False
    return True


def part2(input):
    result = 0
    for line in input:
        _, cube_sets = parse_game(line)
        max_red = 0
        max_green = 0
        max_blue = 0
        for cube_set in cube_sets:
            max_red = max(cube_set['red'], max_red)
            max_green = max(cube_set['green'], max_green)
            max_blue = max(cube_set['blue'], max_blue)
        result += max_red * max_green * max_blue
    return result


def parse_game(line):
    head, tail = line.strip().split(":")
    game_num = int(head.split(" ")[-1])
    cube_sets = []
    for part in tail.strip().split(";"):
        cube_set = defaultdict(int)
        for cube in part.strip().split(","):
            amount, color = cube.strip().split(" ")
            cube_set[color] = int(amount)
        cube_sets.append(cube_set)
    return game_num, cube_sets


if __name__ == "__main__":
    print(part1(open("input.txt")))
    print(part2(open("input.txt")))


from io import StringIO
from unittest import TestCase


class TestStringMethods(TestCase):
    def test_part1(self):
        test_input = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""
        assert 8 == part1(StringIO(test_input))

    def test_part2(self):
        test_input = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""
        assert 2286 == part2(StringIO(test_input))
