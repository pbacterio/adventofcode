#!/usr/bin/env python3
from re import compile


def part1(input):
    re_digits = compile(r"\D")
    result = 0
    for line in input:
        digits = re_digits.sub("", line)
        result += int(digits[0] + digits[-1])
    return result


def part2(input):
    digit2int = dict(zip("123456789", range(1, 10)))
    digit2int.update(
        zip(
            [
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine",
            ],
            range(1, 10),
        )
    )
    re_digits = compile("(?=(" + "|".join(digit2int) + "))")
    result = 0
    for line in input:
        digits = re_digits.findall(line)
        result += digit2int[digits[0]] * 10 + digit2int[digits[-1]]
    return result


if __name__ == "__main__":
    print(part1(open("input1.txt")))
    print(part2(open("input2.txt")))


from io import StringIO
from unittest import TestCase


class TestStringMethods(TestCase):
    def test_part1(self):
        test_input = """1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"""
        assert 142 == part1(StringIO(test_input))

    def test_part2(self):
        test_input = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""
        assert 281 == part2(StringIO(test_input))
