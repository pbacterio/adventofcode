package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func solve() {
	input := openInput()
	input.Scan()
	population := [9]int{} // population by age
	for _, v := range strings.Split(input.Text(), ",") {
		age, _ := strconv.Atoi(v)
		population[age]++
	}

	for day := 0; day < 80; day++ {
		zero := population[0]
		for age := 0; age < 8; age++ {
			population[age] = population[age+1]
		}
		population[6] += zero
		population[8] = zero
	}

	total_population := 0
	for _, p := range population {
		total_population += p
	}
	fmt.Println(total_population)

	for day := 0; day < 256-80; day++ {
		zero := population[0]
		for age := 0; age < 8; age++ {
			population[age] = population[age+1]
		}
		population[6] += zero
		population[8] = zero
	}

	total_population = 0
	for _, p := range population {
		total_population += p
	}
	fmt.Println(total_population)
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}

func main() {
	solve()
}
