package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func part1() {
	input := openInput()
	counter := make([]int, 12)
	for input.Scan() {
		line := input.Text()
		for i, v := range line {
			if v == '0' {
				counter[i]--
			} else {
				counter[i]++
			}
		}
	}
	gamma := 0
	epsilon := 0
	for _, v := range counter {
		if v > 0 {
			gamma++
		} else {
			epsilon++
		}
		gamma <<= 1
		epsilon <<= 1
	}
	gamma >>= 1
	epsilon >>= 1
	fmt.Println(gamma * epsilon)
}

func part2() {
	input := openInput()
	oxygen := []string{}
	for input.Scan() {
		oxygen = append(oxygen, input.Text())
	}
	co2 := oxygen

	for pos := 0; pos < 12; pos++ {
		//oxygen
		if len(oxygen) > 1 {
			ones, zeros := split(oxygen, pos)
			if len(ones) >= len(zeros) {
				oxygen = ones
			} else {
				oxygen = zeros
			}
		}
		//co2
		if len(co2) > 1 {
			ones, zeros := split(co2, pos)
			if len(zeros) <= len(ones) {
				co2 = zeros
			} else {
				co2 = ones
			}
		}
	}
	oxgenInt, _ := strconv.ParseInt(oxygen[0], 2, 0)
	co2Int, _ := strconv.ParseInt(co2[0], 2, 0)
	fmt.Println(oxgenInt * co2Int)
}

func split(lines []string, pos int) (ones []string, zeros []string) {
	ones = make([]string, 0)
	zeros = make([]string, 0)
	for _, line := range lines {
		if line[pos] == '0' {
			zeros = append(zeros, line)
		} else {
			ones = append(ones, line)
		}
	}
	return ones, zeros
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}

func main() {
	part1()
	part2()
}
