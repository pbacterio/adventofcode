package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func part1() {
	input := openInput()
	horizontal := 0
	depth := 0
	for input.Scan() {
		line := input.Text()
		parts := strings.Split(line, " ")
		direction := parts[0]
		moves, _ := strconv.Atoi(parts[1])
		switch direction {
		case "forward":
			horizontal += moves
		case "down":
			depth += moves
		case "up":
			depth -= moves
		}
	}
	fmt.Println(horizontal * depth)
}

func part2() {
	input := openInput()
	horizontal := 0
	depth := 0
	aim := 0
	for input.Scan() {
		line := input.Text()
		parts := strings.Split(line, " ")
		direction := parts[0]
		moves, _ := strconv.Atoi(parts[1])
		switch direction {
		case "forward":
			horizontal += moves
			depth += aim * moves
		case "down":
			aim += moves
		case "up":
			aim -= moves
		}
	}
	fmt.Println(horizontal * depth)
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}

func main() {
	part1()
	part2()
}
