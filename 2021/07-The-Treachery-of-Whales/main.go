package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

func solve() {
	input := openInput()
	input.Scan()
	positions := [1000]int{}
	sum := 0
	for i, v := range strings.Split(input.Text(), ",") {
		position, _ := strconv.Atoi(v)
		positions[i] = position
		sum += position
	}

	mean := int(math.Floor(float64(sum) / 1000.0))
	sort.Ints(positions[:])
	median := int(math.Round(float64(positions[499]+positions[500]) / 2))

	fuelToMedian := 0
	fuelToMean := 0
	for _, position := range positions {
		diff := position - median
		if diff < 0 {
			diff = (-diff)
		}
		fuelToMedian += diff
		diff = position - mean
		if diff < 0 {
			diff = (-diff)
		}
		fuelToMean += diff * (diff + 1) / 2
	}
	fmt.Println(fuelToMedian)
	fmt.Println(fuelToMean)

}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}

func main() {
	solve()
}
