package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	caves, start := loadCaves()
	routes := []*Route{NewRoute(start)}
	newRoutes := []*Route{}
	result1 := 0
	for len(routes) > 0 {
		for _, route := range routes {
			for _, newRoute := range route.steps(caves, false) {
				if newRoute.finished() {
					if newRoute.hasSmallCaves() {
						result1++
					}
				} else {
					newRoutes = append(newRoutes, newRoute)
				}
			}
		}
		routes = newRoutes
		newRoutes = []*Route{}
	}
	fmt.Println(result1)

	routes = []*Route{NewRoute(start)}
	newRoutes = []*Route{}
	result2 := 0
	for len(routes) > 0 {
		for _, route := range routes {
			for _, newRoute := range route.steps(caves, true) {
				if newRoute.finished() {
					if newRoute.hasSmallCaves() {
						result2++
					}
				} else {
					newRoutes = append(newRoutes, newRoute)
				}
			}
		}
		routes = newRoutes
		newRoutes = []*Route{}
	}
	fmt.Println(result2)

}

type Route struct {
	caves             []*Cave
	visited           map[string]int
	smallVisitedTwice bool
}

func NewRoute(start *Cave) *Route {
	return &Route{
		caves: []*Cave{start},
		visited: map[string]int{
			start.id: 1,
		},
		smallVisitedTwice: false,
	}
}

func (r *Route) clone() *Route {
	newCaves := make([]*Cave, len(r.caves))
	copy(newCaves, r.caves)
	newVisited := map[string]int{}
	for k, v := range r.visited {
		newVisited[k] = v
	}
	return &Route{
		caves:             newCaves,
		visited:           newVisited,
		smallVisitedTwice: r.smallVisitedTwice,
	}

}

func (r *Route) move(c *Cave) *Route {
	if _, visited := r.visited[c.id]; visited && c.isSmall() && !r.smallVisitedTwice {
		r.smallVisitedTwice = true
	}
	r.caves = append(r.caves, c)
	r.visited[c.id]++
	return r
}

func (r *Route) steps(caves map[string]*Cave, extraTime bool) []*Route {
	routes := []*Route{}
	currCave := r.caves[len(r.caves)-1]
	for _, nextCave := range currCave.connections {
		if nextCave.isStart() {
			continue
		}
		if nextCave.isEnd() {
			newRoute := r.clone().move(nextCave)
			routes = append(routes, newRoute)
			continue
		}
		if nextCave.isSmall() {
			if _, visited := r.visited[nextCave.id]; visited {
				if !extraTime {
					continue
				}
				if r.smallVisitedTwice {
					continue
				}
			}
		}
		newRoute := r.clone().move(nextCave)
		routes = append(routes, newRoute)
	}
	return routes
}

func (r *Route) hasSmallCaves() bool {
	for _, c := range r.caves {
		if c.isSmall() {
			return true
		}
	}
	return false
}

func (r *Route) finished() bool {
	return r.caves[len(r.caves)-1].isEnd()
}

type Cave struct {
	id          string
	connections map[string]*Cave
}

func NewCave(id string) *Cave {
	return &Cave{id, map[string]*Cave{}}
}

func (c *Cave) connect(cave *Cave) {
	c.connections[cave.id] = cave
}

func (c *Cave) isSmall() bool {
	return strings.ToLower(c.id) == c.id
}

func (c *Cave) isStart() bool {
	return c.id == "start"
}

func (c *Cave) isEnd() bool {
	return c.id == "end"
}

func loadCaves() (caveMap map[string]*Cave, start *Cave) {
	input := openInput()
	caves := map[string]*Cave{}
	for input.Scan() {
		parts := strings.Split(input.Text(), "-")
		cave1, ok := caves[parts[0]]
		if !ok {
			cave1 = NewCave(parts[0])
			caves[cave1.id] = cave1
		}
		cave2, ok := caves[parts[1]]
		if !ok {
			cave2 = NewCave(parts[1])
			caves[cave2.id] = cave2
		}
		cave1.connect(cave2)
		cave2.connect(cave1)
	}
	return caves, caves["start"]
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}
