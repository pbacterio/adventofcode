package main

import (
	"bufio"
	"fmt"
	"os"
)

type OctopusGrid struct {
	energy [10][10]int
}

func LoadOctopusGrid() *OctopusGrid {
	og := OctopusGrid{}
	input := openInput()
	for x := 0; input.Scan(); x++ {
		for y, char := range input.Text() {
			og.energy[x][y] = int(char) - '0'
		}
	}
	for x := 0; input.Scan(); x++ {
		for y, char := range input.Text() {
			og.energy[x][y] = int(char) - '0'
		}
	}
	return &og
}

func (og *OctopusGrid) step() (flashes int) {
	for x := 0; x < 10; x++ {
		for y := 0; y < 10; y++ {
			og.energy[x][y]++
		}
	}
	anyFlash := true
	for anyFlash {
		anyFlash = false
		for x := 0; x < 10; x++ {
			for y := 0; y < 10; y++ {
				if og.energy[x][y] > 9 {
					og.energy[x][y] = -1
					flashes++
					anyFlash = true
					for _, xy := range getAdjacent(x, y) {
						if og.energy[xy[0]][xy[1]] >= 0 {
							og.energy[xy[0]][xy[1]]++
						}
					}
				}
			}
		}
	}
	for x := 0; x < 10; x++ {
		for y := 0; y < 10; y++ {
			if og.energy[x][y] < 0 {
				og.energy[x][y] = 0
			}
		}
	}
	return flashes
}

func (og *OctopusGrid) String() string {
	result := ""
	for x := 0; x < 10; x++ {
		if len(result) > 0 {
			result += "\n"
		}
		result += fmt.Sprintf("%d%d%d%d%d%d%d%d%d%d", og.energy[x][0], og.energy[x][1], og.energy[x][2], og.energy[x][3], og.energy[x][4], og.energy[x][5], og.energy[x][6], og.energy[x][7], og.energy[x][8], og.energy[x][9])
	}
	return result
}

func getAdjacent(x int, y int) [][2]int {
	all := [][2]int{
		{x - 1, y - 1},
		{x - 1, y},
		{x - 1, y + 1},
		{x, y - 1},
		{x, y + 1},
		{x + 1, y - 1},
		{x + 1, y},
		{x + 1, y + 1},
	}
	adjacent := [][2]int{}
	for _, xy := range all {
		if xy[0] >= 0 && xy[0] < 10 && xy[1] >= 0 && xy[1] < 10 {
			adjacent = append(adjacent, xy)
		}
	}
	return adjacent
}

func main() {
	og := LoadOctopusGrid()
	totalFlashes := 0
	for i := 0; i < 100; i++ {
		flashes := og.step()
		totalFlashes += flashes
	}
	fmt.Println(totalFlashes)
	for i := 101; true; i++ {
		if og.step() == 100 {
			fmt.Println(i)
			break
		}
	}
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}
