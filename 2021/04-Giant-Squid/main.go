package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func solve(random []int, boards [100]Board) {
	part1_score := 0
	part2_score := 0
	for _, num := range random {
		for i := 0; i < 100; i++ {
			score := boards[i].Mark(num)
			if score > 0 {
				if part1_score == 0 {
					part1_score = score
				}
				boards[i].disabled = true
				part2_score = score
			}
		}
	}
	fmt.Println(part1_score)
	fmt.Println(part2_score)
}

type Board struct {
	grid     [5][5]int
	row_stat [5]int // Row mark counters
	col_stat [5]int // Col mark counters
	disabled bool
}

func (b *Board) Mark(num int) int {
	if b.disabled {
		return 0
	}
	for r, col := range b.grid {
		for c, v := range col {
			if v == num {
				b.grid[r][c] = -1 // Mark
				b.row_stat[r]++
				b.col_stat[c]++
				if b.row_stat[r] == 5 || b.col_stat[c] == 5 {
					return b.sumUnmarked() * num
				}
			}
		}
	}
	return 0
}

func (b *Board) sumUnmarked() int {
	sum := 0
	for _, row := range b.grid {
		for _, v := range row {
			if v > 0 {
				sum += v
			}
		}
	}
	return sum
}

func (b *Board) load(input *bufio.Scanner) {
	for r := 0; r < 5; r++ {
		input.Scan()
		line := strings.TrimLeft(input.Text(), " ")
		lineParts := regexp.MustCompile(" +").Split(line, -1)
		for c, v := range strArr2intArr(lineParts) {
			b.grid[r][c] = v
		}
	}
}

func main() {
	input := openInput()

	input.Scan()
	random := strArr2intArr(strings.Split(input.Text(), ","))

	boards := [100]Board{}
	for i := 0; i < 100; i++ {
		input.Scan()
		boards[i].load(input)
	}

	solve(random, boards)
}

func loadRandon(input *bufio.Scanner) []int {
	return strArr2intArr(strings.Split(input.Text(), ","))
}

func strArr2intArr(strArr []string) []int {
	intArr := make([]int, len(strArr))
	for i, v := range strArr {
		r, _ := strconv.Atoi(v)
		intArr[i] = r
	}
	return intArr
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}
