package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	input := []int{}

	inputFile, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inputScanner := bufio.NewScanner(inputFile)
	for inputScanner.Scan() {
		measure, _ := strconv.Atoi(inputScanner.Text())
		input = append(input, measure)
	}

	increases := 0
	previousWindowSum := input[0] + input[1] + input[2]
	for i := 3; i < len(input); i++ {
		currentWindowSum := previousWindowSum + input[i] - input[i-3]
		if currentWindowSum > previousWindowSum {
			increases++
		}
		previousWindowSum = currentWindowSum
	}

	fmt.Println(increases)
}
