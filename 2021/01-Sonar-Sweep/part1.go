package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
)

func main() {
	inputFile, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inputScanner := bufio.NewScanner(inputFile)

	previousMeasure := math.MaxInt
	increases := 0
	for inputScanner.Scan() {
		measure, _ := strconv.Atoi(inputScanner.Text())
		if measure > previousMeasure {
			increases++
		}
		previousMeasure = measure
	}

	fmt.Println(increases)
}
