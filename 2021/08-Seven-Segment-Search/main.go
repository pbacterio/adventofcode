package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func solve() {
	part1_result := 0
	part2_result := 0
	scoresToDigit := map[int]int{
		42: 0,
		17: 1,
		34: 2,
		39: 3,
		30: 4,
		37: 5,
		41: 6,
		25: 7,
		49: 8,
		45: 9,
	}
	input := openInput()
	for input.Scan() {
		samples, outputs := parseInput(input.Text())
		ledFreq := map[byte]int{'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0, 'f': 0, 'g': 0}
		for _, sample := range samples {
			for _, char := range sample {
				ledFreq[byte(char)]++
			}
		}
		result := 0
		for _, output := range outputs {
			score := 0
			for _, char := range output {
				score += ledFreq[byte(char)]
			}
			digit := scoresToDigit[score]
			result *= 10
			result += digit
			if digit == 1 || digit == 4 || digit == 7 || digit == 8 {
				part1_result += 1
			}
		}
		part2_result += result
	}
	fmt.Println(part1_result)
	fmt.Println(part2_result)
}

func parseInput(line string) (samples []string, output []string) {
	parts := strings.Split(line, "|")
	samples = strings.Split(strings.TrimRight(parts[0], " "), " ")
	output = strings.Split(strings.TrimLeft(parts[1], " "), " ")
	return samples, output
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}

func main() {
	solve()
}
