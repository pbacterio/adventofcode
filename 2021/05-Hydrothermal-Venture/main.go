package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Point struct {
	x int
	y int
}

func solve(lines [][2]Point) {
	space := map[Point]int{}
	for _, line := range lines {
		p1, p2 := line[0], line[1]
		if p1.x == p2.x { // Horizontal
			y1, y2 := p1.y, p2.y
			if y1 > y2 {
				y1, y2 = y2, y1
			}
			for ; y1 <= y2; y1++ {
				space[Point{p1.x, y1}]++
			}
		} else if p1.y == p2.y { // Vertical
			x1, x2 := p1.x, p2.x
			if x1 > x2 {
				x1, x2 = x2, x1
			}
			for ; x1 <= x2; x1++ {
				space[Point{x1, p1.y}]++
			}
		}
	}
	result1 := 0
	for _, covers := range space {
		if covers > 1 {
			result1++
		}
	}
	fmt.Println(result1)
	for _, line := range lines {
		p1, p2 := line[0], line[1]
		if p1.x == p2.x || p1.y == p2.y {
			continue
		}
		x, y := 0, 0
		for i := 0; i <= abs(p1.x-p2.x); i++ {
			if p1.x < p2.x {
				x = p1.x + i
			} else {
				x = p1.x - i
			}
			if p1.y < p2.y {
				y = p1.y + i
			} else {
				y = p1.y - i
			}
			space[Point{x, y}]++
		}
	}
	result2 := 0
	for _, covers := range space {
		if covers > 1 {
			result2++
		}
	}
	fmt.Println(result2)

}

func abs(n int) int {
	if n < 0 {
		return -n
	}
	return n
}

func parseLine(line string) (p1 Point, p2 Point) {
	lineParts := strings.SplitN(line, " -> ", 2)
	point1Parts := strings.SplitN(lineParts[0], ",", 2)
	point2Parts := strings.SplitN(lineParts[1], ",", 2)
	p1.x, _ = strconv.Atoi(point1Parts[0])
	p1.y, _ = strconv.Atoi(point1Parts[1])
	p2.x, _ = strconv.Atoi(point2Parts[0])
	p2.y, _ = strconv.Atoi(point2Parts[1])
	return p1, p2
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}

func main() {
	input := openInput()
	lines := [][2]Point{}
	for input.Scan() {
		p1, p2 := parseLine(input.Text())
		lines = append(lines, [2]Point{p1, p2})
	}
	solve(lines)
}
