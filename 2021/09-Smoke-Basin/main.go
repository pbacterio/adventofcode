package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
)

type Point struct {
	x int
	y int
}

type Basin struct {
	LowPoint Point
	Points   map[Point]bool
	border   []Point
}

func NewBasin(p Point) *Basin {
	return &Basin{p, map[Point]bool{p: true}, []Point{p}}
}

func (b *Basin) Grow(heightmap [][]int) {
	for len(b.border) > 0 {
		newBorder := []Point{}
		for _, borderPoint := range b.border {
			for _, adjacent := range getAdjacentPoints(borderPoint) {
				_, inBasin := b.Points[adjacent]
				if inBasin {
					continue
				}
				if heightmap[adjacent.x][adjacent.y] == 9 {
					continue
				}
				b.Points[adjacent] = true
				newBorder = append(newBorder, adjacent)
			}
			b.border = newBorder
		}
	}
}

func main() {
	heightmap := [][]int{} // x,y ==> height

	input := openInput()
	for input.Scan() {
		heightline := []int{}
		for _, char := range input.Text() {
			height, _ := strconv.Atoi(string(char))
			heightline = append(heightline, height)
		}
		heightmap = append(heightmap, heightline)
	}

	result1 := 0
	basins := []Basin{}
	for x := 0; x < 100; x++ {
		for y := 0; y < 100; y++ {
			adjMin := math.MaxInt
			for _, adjacent := range getAdjacentPoints(Point{x, y}) {
				if heightmap[adjacent.x][adjacent.y] < adjMin {
					adjMin = heightmap[adjacent.x][adjacent.y]
				}
			}
			if heightmap[x][y] < adjMin {
				result1 += heightmap[x][y] + 1
				basin := NewBasin(Point{x, y})
				basin.Grow(heightmap)
				basins = append(basins, *basin)
			}
		}
	}
	fmt.Println(result1)

	sort.Slice(basins, func(i, j int) bool {
		return len(basins[i].Points) > len(basins[j].Points)
	})
	fmt.Println(len(basins[0].Points) * len(basins[1].Points) * len(basins[2].Points))

}

func getAdjacentPoints(p Point) []Point {
	adjacent := []Point{}
	if p.x > 0 {
		adjacent = append(adjacent, Point{p.x - 1, p.y})
	}
	if p.x < 99 {
		adjacent = append(adjacent, Point{p.x + 1, p.y})
	}
	if p.y > 0 {
		adjacent = append(adjacent, Point{p.x, p.y - 1})
	}
	if p.y < 99 {
		adjacent = append(adjacent, Point{p.x, p.y + 1})
	}
	return adjacent
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}
