package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

func main() {
	result1 := 0
	scores := []int{}
	input := openInput()
	for input.Scan() {
		openings := []rune{}
		corrupted := false
		for _, char := range input.Text() {
			if !isClose(char) {
				openings = append(openings, char)
				continue
			}
			lastOpening := openings[len(openings)-1]
			if endsMatch(lastOpening, char) {
				openings = openings[:len(openings)-1]
			} else {
				result1 += errorValue[char]
				corrupted = true
				break
			}
		}
		if corrupted {
			continue
		}
		score := 0
		for i := len(openings) - 1; i >= 0; i-- {
			score = score*5 + closeValue[openings[i]]
		}
		scores = append(scores, score)
	}
	fmt.Println(result1)
	sort.Ints(scores)
	fmt.Println(scores[len(scores)/2])
}

var errorValue = map[rune]int{
	')': 3,
	']': 57,
	'}': 1197,
	'>': 25137,
}

var closeValue = map[rune]int{
	'(': 1,
	'[': 2,
	'{': 3,
	'<': 4,
}

func isClose(c rune) bool {
	_, r := errorValue[c]
	return r
}

func endsMatch(opening rune, closing rune) bool {
	if opening == '(' && closing == ')' {
		return true
	}
	if opening == '[' && closing == ']' {
		return true
	}
	if opening == '{' && closing == '}' {
		return true
	}
	if opening == '<' && closing == '>' {
		return true
	}
	return false
}

func openInput() *bufio.Scanner {
	f, _ := os.Open("input")
	return bufio.NewScanner(f)
}
