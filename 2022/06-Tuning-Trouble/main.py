#!/usr/bin/env python3

from collections import Counter, deque


def part1(stram):
    print(search_start(stream, 4))


def part2(stream):
    print(search_start(stream, 14))


def search_start(stream, mark_length):
    buffer = deque()
    counter = Counter()
    for index, char in enumerate(stream):
        buffer.append(char)
        counter[char] += 1
        if len(buffer) <= mark_length:
            continue
        leaving_char = buffer.popleft()
        counter[leaving_char] -= 1
        if counter[leaving_char] == 0:
            del counter[leaving_char]
        if len(counter) == mark_length:
            return index+1


if __name__ == '__main__':
    stream = open('input').readline().rstrip()
    part1(stream)
    part2(stream)
