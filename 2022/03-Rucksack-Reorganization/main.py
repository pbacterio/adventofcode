#!/usr/bin/env python3

from collections import Counter


def part1():
    total_priority = 0
    for line in map(str.strip, open('input')):
        comp1 = {item: 1 for item in line[:len(line)//2]}
        for item in line[len(line)//2:]:
            if item in comp1:
                total_priority += priority(item)
                break
    print(total_priority)


def part2():
    total_priority = 0
    for group in groups_reader():
        item_count = Counter()
        for items in group:
            item_count.update(uniq(items))
        total_priority += priority(item_count.most_common(1)[0][0])
    print(total_priority)


def uniq(items):
    uniq_items = {}
    for item in items:
        uniq_items[item] = True
    return uniq_items.keys()


def groups_reader():
    group = []
    for line in map(str.strip, open('input')):
        group.append(line)
        if len(group) == 3:
            yield group
            group = []


def priority(item):
    if item.islower():
        return ord(item)-96
    else:
        return ord(item)-38


if __name__ == '__main__':
    part1()
    part2()
