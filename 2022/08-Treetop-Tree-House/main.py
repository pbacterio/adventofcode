#!/usr/bin/env python3

from functools import cache
from itertools import product


class Forest:

    def __init__(self):
        self.treesmap = []  # [row][col]
        for line in map(str.rstrip, open('input')):
            self.treesmap.append([int(t) for t in line])

    def visible_trees(self):
        count = len(self.treesmap)*2 + len(self.treesmap[0])*2 - 4
        for row, col in product(range(1, len(self.treesmap)-1), range(1, len(self.treesmap[0])-1)):
            height = self.treesmap[row][col]
            if height > max(self.treesmap[row][:col])\
                    or height > max(self.treesmap[row][col+1:])\
                    or height > max(self._get_col(col)[:row]) \
                    or height > max(self._get_col(col)[row+1:]):
                count += 1
        return count

    @cache
    def _get_col(self, idx):
        return [r[idx] for r in self.treesmap]

    def best_visibility_score(self):
        best_score = 0
        for row, col in product(range(1, len(self.treesmap)-1), range(1, len(self.treesmap[0])-1)):
            height = self.treesmap[row][col]
            score = _visibility_score(
                height, self.treesmap[row][col+1:])  # rigth
            # left
            score *= _visibility_score(height,
                                       reversed(self.treesmap[row][:col]))
            score *= _visibility_score(height,
                                       self._get_col(col)[row+1:])  # down
            score *= _visibility_score(height,
                                       reversed(self._get_col(col)[:row]))  # up
            best_score = max(score, best_score)
        return best_score


def _visibility_score(height, view):
    score = 0
    for h in view:
        score += 1
        if h >= height:
            break
    return score


if __name__ == '__main__':
    forest = Forest()
    print(forest.visible_trees())  # part 1
    print(forest.best_visibility_score())  # part 2
