#!/usr/bin/env python

# A X - Rock
# B Y - Paper
# C Z - Scissors

round_score = {
    'A X': (1+3, 3),
    'A Y': (2+6, 1+3),
    'A Z': (3, 2+6),
    'B X': (1, 1),
    'B Y': (2+3, 2+3),
    'B Z': (3+6, 3+6),
    'C X': (1+6, 2),
    'C Y': (2, 3+3),
    'C Z': (3+3, 1+6),
}


def part1():
    total_score = 0
    for round in map(str.strip, open('input')):
        total_score += round_score[round][0]
    print(total_score)


def part2():
    total_score = 0
    for round in map(str.strip, open('input')):
        total_score += round_score[round][1]
    print(total_score)


if __name__ == '__main__':
    part1()
    part2()
