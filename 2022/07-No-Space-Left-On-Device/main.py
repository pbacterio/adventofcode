#!/usr/bin/env python3

from collections import deque, Counter

def solve():
    dirs = Counter()
    path = deque()
    cur_dir_size = 0
    for line in open('input'):
        line_parts = line.rstrip().split()
        if line_parts[0] == '$':
            if line_parts[1] == 'cd':
                # Update parents total_size before change
                for directory in path:
                    dirs[directory] += cur_dir_size
                # Change directory
                directory = line_parts[2]
                if directory == '/':
                    path.clear()
                    path.append('/')
                elif directory == '..':
                    path.pop()
                else:
                    if len(path):
                        directory=path[-1]+'/'+directory
                    path.append(directory)

                cur_dir_size = dirs[directory]
        elif line_parts[0] == 'dir':
            pass
        else:
            cur_dir_size += int(line_parts[0])
    for directory in path:
        dirs[directory] += cur_dir_size

    # part 1
    size_sum = 0
    for directory in dirs:
        dir_size = dirs[directory]
        if dir_size <= 100000:
            size_sum += dir_size
    print(size_sum)

    # part 2
    need_to_free =  30000000 - (70000000 - dirs['/'])
    min_size = dirs['/']
    for directory in dirs:
        dir_size = dirs[directory]
        if dir_size >= need_to_free:
            if dir_size < min_size:
                min_size = dir_size
    print(min_size)

if __name__ == '__main__':
    solve()
