#!/usr/bin/env python

from collections import deque


def part1():
    input_file = open('input')
    stacks = load_stacks(input_file)
    for line in input_file:
        _, amount, _, src, _, dst = line.split(' ')
        for _ in range(int(amount)):
            stacks[int(dst)-1].append(stacks[int(src)-1].pop())
    result = []
    for stack in stacks:
        result.append(stack.pop())
    print(''.join(result))


def part2():
    input_file = open('input')
    stacks = load_stacks(input_file)
    for line in input_file:
        _, amount, _, src, _, dst = line.split(' ')
        dst_stack_len = len(stacks[int(dst)-1])
        for _ in range(int(amount)):
            stacks[int(dst)-1].insert(dst_stack_len, stacks[int(src)-1].pop())
    result = []
    for stack in stacks:
        result.append(stack.pop())
    print(''.join(result))


def load_stacks(input_file):
    stacks = [deque() for _ in range(9)]
    for _ in range(8):
        line = input_file.readline()
        for si in range(9):
            crate = line[si*4+1]
            if crate != ' ':
                stacks[si].appendleft(crate)
    input_file.readline()
    input_file.readline()
    return stacks


if __name__ == '__main__':
    part1()
    part2()