#!/usr/bin/env python3

import regex


def part1():
    count = 0
    for elf1, elf2 in sections_reader():
        if elf1[0] <= elf2[0] and elf1[1] >= elf2[1]:
            count += 1
        elif elf2[0] <= elf1[0] and elf2[1] >= elf1[1]:
            count += 1
    print(count)


def part2():
    count = 0
    for elf1, elf2 in sections_reader():
        if elf1[0] <= elf2[0] <= elf1[1]:
            count += 1
        elif elf2[0] <= elf1[0] <= elf2[1]:
            count += 1
    print(count)


def sections_reader():
    for line in map(str.strip, open('input')):
        nums = list(map(int, regex.split(',|-', line)))
        yield ((nums[0], nums[1]), (nums[2], nums[3]))


if __name__ == '__main__':
    part1()
    part2()
