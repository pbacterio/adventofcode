#!/usr/bin/env python3
from heapq import heappushpop


def part1():
    max_calories = 0
    for elf_calories in calories_reader():
        max_calories = max(elf_calories, max_calories)
    print(max_calories)


def part2():
    reader = calories_reader()
    top = [next(reader), next(reader), next(reader)]
    for elf_calories in reader:
        heappushpop(top, elf_calories)
    print(sum(top))


def calories_reader():
    elf_calories = 0
    for line in map(str.strip, open('input')):
        if line:
            elf_calories += int(line)
        else:
            yield elf_calories
            elf_calories = 0
    if elf_calories:
        yield elf_calories


if __name__ == '__main__':
    part1()
    part2()
